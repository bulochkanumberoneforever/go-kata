package main

import (
	"fmt"
)

type Collection struct {
	People []Userer
}

type User struct {
	ID   int
	Name string
}

func (u *User) GetName() string {
	return u.Name
}

type Userer interface {
	GetName() string
}

func main() {
	//u := &[]User{
	//	{
	//		ID:   34,
	//		Name: "Annet",
	//	},
	//	{
	//		ID:   55,
	//		Name: "John",
	//	},
	//	{
	//		ID:   89,
	//		Name: "Alex",
	//	},
	//}
	collection := new(Collection)
	annet := User{
		ID:   12,
		Name: "annet",
	}
	john := User{
		ID:   13,
		Name: "asfa",
	}
	alex := User{
		ID:   89,
		Name: "Alex",
	}
	collection.People = []Userer{&annet, &john, &alex}
	users := collection.People
	testUserName(users)
}

func testUserName(users []Userer) {
	for _, u := range users {
		fmt.Println(u.GetName())
	}
}
