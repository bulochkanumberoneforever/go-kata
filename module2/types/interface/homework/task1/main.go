package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var m MyInterface
	var n *int
	fmt.Println(n == nil)
	test(m)
}

func test(r interface{}) {
	values := r
	switch values.(type) {
	case nil:
		fmt.Println("Success!")
	}
}
