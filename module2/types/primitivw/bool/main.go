package main

import (
	"fmt"
	"unsafe"
)

func main() {
	n := 112358132134
	fmt.Println("Size is", unsafe.Sizeof(n), "bytes")
	typeBool()
}

func typeBool() {
	var b bool
	fmt.Println("Size in bytes is", unsafe.Sizeof(b))
	var u uint8 = 1
	fmt.Println(b)
	b = *(*bool)(unsafe.Pointer(&u))
	fmt.Println(b)
}
