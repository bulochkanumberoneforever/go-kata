package main

import "fmt"

func main() {
	p0 := new(int)
	x := *p0
	p1 := &x
	p3 := &*p0
	fmt.Println(&x, p0, p1, p3)
}
