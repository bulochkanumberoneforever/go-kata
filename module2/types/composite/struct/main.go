package main

import (
	"fmt"
	"unsafe"
)

type User struct {
	Age  int
	Name string
	Wallet
	Location
}

type Wallet struct {
	RUR uint64
	BTC uint64
	USD uint64
	ETH uint64
}

type Location struct {
	Address string
	City    string
	Index   string
}

func main() {
	user := User{
		Age:  13,
		Name: "Sasha",
	}
	wallet := Wallet{
		RUR: 25000,
		BTC: 4,
		USD: 3500,
		ETH: 1,
	}
	user.Wallet = wallet
	fmt.Println(wallet)
	fmt.Println("wallet allocoast", unsafe.Sizeof(wallet), "bytes")
	fmt.Println(user)
	user2 := User{
		Age:  19,
		Name: "Elena",
		Wallet: Wallet{
			RUR: 123123,
			BTC: 12313,
			USD: 123,
			ETH: 12,
		},
		Location: Location{
			Address: "Energy",
			City:    "Moscow",
			Index:   "601501",
		},
	}
	fmt.Println(user2)
}
