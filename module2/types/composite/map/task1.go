package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/golang/go",
			Stars: 108000,
		},
		{
			Name:  "https://github.com/datasciencemasters/go",
			Stars: 25000,
		},
		{
			Name:  "https://github.com/TheAlgorithms/Go",
			Stars: 19000,
		},
		{
			Name:  "https://github.com/json-iterator/go",
			Stars: 17000,
		},
		{
			Name:  "https://github.com/xinliangnote/Go",
			Stars: 3100,
		},
		{
			Name:  "https://github.com/stellar/go",
			Stars: 1200,
		},
		{
			Name:  "https://github.com/exercism/go",
			Stars: 780,
		},
		{
			Name:  "https://github.com/jenkinsci/docker",
			Stars: 5700,
		},
		{
			Name:  "https://github.com/nextcloud/docker",
			Stars: 4500,
		},
		{
			Name:  "https://github.com/sous-chefs/docker",
			Stars: 1300,
		},
		{
			Name:  "https://github.com/docker-library/docker",
			Stars: 801,
		},
		{
			Name:  "https://github.com/matomo-org/docker",
			Stars: 685,
		},
	}
	map1 := make(map[string]int)
	for i := range projects {
		map1[projects[i].Name] = projects[i].Stars
	}
	for _, value := range map1 { // Порядок не определен
		fmt.Println(value)
	}
}
