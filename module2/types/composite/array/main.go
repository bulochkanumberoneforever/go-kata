package main

import "fmt"

type User struct {
	Age  int
	Name string
	Wallet
}

type Wallet struct {
	RUR uint64
	BTC uint64
	USD uint64
	ETH uint64
}

func main() {
	rangeArray()
}

func testArray() {
	a := [...]int{1, 3, 4}
	fmt.Println("original values", a)
	a[0] = 23
	fmt.Println("new values", a)
	b := a
	a[0] = 56
	fmt.Println("past array", a, "new array", b)
}

func rangeArray() {
	users := [4]User{
		{Age: 16,
			Name: "Masha",
			Wallet: Wallet{
				RUR: 123123,
				BTC: 12313,
				USD: 123,
				ETH: 12,
			},
		},
		{Age: 12,
			Name: "Dima",
			Wallet: Wallet{
				RUR: 123123,
				BTC: 12313,
				USD: 123,
				ETH: 12,
			},
		},
		{Age: 23,
			Name: "Sasha",
			Wallet: Wallet{
				RUR: 123123,
				BTC: 12313,
				USD: 123,
				ETH: 12,
			},
		},
	}
	fmt.Println("User older then 18 years:")
	for i := range users {
		if users[i].Age > 18 {
			fmt.Println(users[i])
		}
	}
	fmt.Println("User who have more than 0 BTC:")
	for i := range users {
		if users[i].BTC > 0 {
			fmt.Println(users[i])
		}
	}
}
