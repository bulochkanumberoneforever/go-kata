package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Alice",
			Age:  21,
		},
		{
			Name: "John",
			Age:  34,
		},
		{
			Name: "Alexander",
			Age:  45,
		},
		{
			Name: "Ivan",
			Age:  13,
		},
		{
			Name: "Denis",
			Age:  44,
		},
		{
			Name: "Mary",
			Age:  26,
		},
		{
			Name: "Rose",
			Age:  41,
		},
	}
	var s []int
	for i, _ := range users {
		if users[i].Age > 40 {
			s = append(s, i)
		}
	}

	users = append(users[:s[0]], users[s[0]+1:]...)
	users = append(users[:s[1]-1], users[s[1]:]...)
	users = append(users[:s[2]-2], users[s[2]-1:]...)
	fmt.Println(users, s)
}
