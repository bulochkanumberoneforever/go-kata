package main

import "fmt"

func main() {
	slice := []int{0, 1, 2, 3}
	fmt.Println(slice)
	slice = Append(slice)
	fmt.Println(slice)
}

// Append добавляет элементы в срез.
// Первая версия: просто зациклить вызов Extend.
func Append(slice []int) []int {
	for i := range slice {
		slice[i]++
	}
	return slice
}
