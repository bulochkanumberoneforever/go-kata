package main

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"sync"
	"time"
)

func isClosed(c chan int) bool {
	select {
	case <-c:
		return true
	default:
	}
	return false
}

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()
	ctx, cancelCtx := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancelCtx()
	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				log.Printf("ctx done: %v", ctx.Err())
				close(a)
				return
			default:
				log.Println("working...")
				time.Sleep(1 * time.Second)
				a <- num
			}
		}
	}()
	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				log.Printf("ctx done: %v", ctx.Err())
				close(b)
				return
			default:
				log.Println("working...")
				time.Sleep(1 * time.Second)
				b <- num
			}
		}
	}()

	go func() {
		for num := range out {
			select {
			case <-ctx.Done():
				log.Printf("ctx done: %v", ctx.Err())
				close(c)
				return
			default:
				log.Println("working...")
				time.Sleep(1 * time.Second)
				c <- num
			}
		}
	}()
	mainChan := joinChannels(a, b, c)
	for num := range mainChan {
		fmt.Println(num)
	}

	fmt.Println(isClosed(a), isClosed(mainChan))

}

//func doSomething(ctx context.Context) {
//	deadline := time.Now().Add(1500 * time.Millisecond)
//	ctx, cancelCtx := context.WithDeadline(ctx, deadline)
//	defer cancelCtx()
//
//	printCh := make(chan int)
//
//	for num := 1; num <= 3; num++ {
//		select {
//		case printCh <- num:
//			time.Sleep(1 * time.Second)
//		case <-ctx.Done():
//			break
//		}
//	}
//
//	cancelCtx()
//
//	time.Sleep(100 * time.Millisecond)
//
//	fmt.Printf("doSomething: finished\n")
//}
