package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func isClosed(c chan int) bool {
	select {
	case <-c:
		return true
	default:
	}
	return false
}

//func joinChannels(chs ...<-chan int) chan int {
//	mergeChannel := make(chan int)
//	go func() {
//		wg := new(sync.WaitGroup)
//		for _, i := range chs {
//			wg.Add(1)
//			go func(i <-chan int) {
//				for v := range i {
//					mergeChannel <- v
//				}
//				wg.Done()
//			}(i)
//		}
//		wg.Wait()
//		close(mergeChannel)
//	}()
//	return mergeChannel
//}
//
////
////	func joinChannels(chs ...<-chan int) chan int {
////		var wg sync.WaitGroup
////
////		mergeChannel := make(chan int)
////		for _, k := range chs {
////			wg.Add(1)
////			go func(k <-chan int) {
////				for v := range k {
////					mergeChannel <- v
////				}
////				wg.Done()
////			}(k)
////		}
////		go func() {
////			wg.Wait()
////			close(mergeChannel)
////		}()
////		return mergeChannel
////	}
//

func joinChannels(chs ...<-chan int) chan int {

	mergeChannel := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}
		for _, v := range chs {
			wg.Add(1)
			go func() {
				defer wg.Done()
				for i := range v {
					mergeChannel <- i

				}
			}()
		}
		wg.Wait()
	}()
	close(mergeChannel)
	return mergeChannel
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	mainChan := joinChannels(a, b, c)
	fmt.Println(mainChan, isClosed(mainChan))
}

//
//func main() {
//	writes := 1000
//	m := make(map[int]int, writes)
//	wg := &sync.WaitGroup{}
//
//	for i := 0; i < writes; i++ {
//		wg.Add(1)
//		go func(i int) {
//			defer wg.Done()
//			m[i] = i
//		}(i)
//	}
//	go func() {
//		wg.Wait()
//	}()
//	fmt.Println(m)
//}
