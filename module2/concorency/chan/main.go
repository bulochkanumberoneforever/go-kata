package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func isClosed(c chan int) bool {
	select {
	case <-c:
		return true
	default:
	}
	return false
}

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData() chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case _, ok := <-out:
				if !ok {
					return
				}
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData()

	//go func() {
	//	for num := range out {
	//		a <- num
	//	}
	//	close(a)
	//}()
	//
	//go func() {
	//	for num := range out {
	//		b <- num
	//	}
	//	close(b)
	//}()
	go func() {
		for num := range out {
			a <- num
		}
		close(a)
	}()

	go func() {
		for num := range out {
			b <- num
		}
		close(b)
	}()

	go func() {
		for num := range out {
			c <- num
		}
		close(c)
	}()

	ticker := time.NewTicker(time.Second)
	done := make(chan bool)
	go func() {
		for {
			select {
			case <-done:
				return
			case t := <-ticker.C:
				fmt.Println("Tick at", t)
			}
		}
	}()

	time.Sleep(3 * time.Second)

	ticker.Stop()
	done <- true
	fmt.Println("Ticker stopped")

	mainChan := joinChannels(a, b, c)

	//for num := range joinChannels(a, b, c) {
	//	fmt.Println(num)
	//}
	time.Sleep(3 * time.Second)
	fmt.Println(isClosed(mainChan), isClosed(a))
}
