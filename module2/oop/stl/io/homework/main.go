package main

// You can edit this code!
// Click here and start typing.

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

func main() {
	//data := []string{
	//	"there is 3pm, but im still alive to write this code snippet",
	//	"чистый код лучше, чем заумный код",
	//	"ваш код станет наследием будущих программистов",
	//	"задумайтесь об этом",

	buf := new(bytes.Buffer) // здесь расположите буфер
	buf.WriteString("there is 3pm, but im still alive to write this code snippet\n")
	buf.WriteString("чистый код лучше, чем заумный код\n")
	buf.WriteString("ваш код станет наследием будущих программистов\n")
	buf.WriteString("задумайтесь об этом\n")
	file, err := os.Create("example.txt")
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	} // создайте файл

	if _, err := io.Copy(file, buf); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	defer file.Close() // запишите данные в файл
	file1, err1 := os.Open("example.txt")
	if err1 != nil {
		fmt.Println("Unable to open file:", err)
		return
	}
	defer file1.Close()

	reader := bufio.NewReader(file1)
	for {
		line, err := reader.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				fmt.Println(err)
				return
			}
		}
		fmt.Print(line)
	}
}

// у вас все получится!
