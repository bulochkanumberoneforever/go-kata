package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
)

type Config struct {
	AppName    string
	Production bool
}

func main() {
	configPath := flag.String("conf", "", "Path to the config file")
	flag.Parse()
	configBytes, err := ioutil.ReadFile(*configPath)
	conf := Config{}
	err1 := json.Unmarshal(configBytes, &conf)
	if err1 != nil {
		log.Fatal("Unmarshal failed", err)
	}
	fmt.Println(conf.AppName)
	fmt.Println(conf.Production)
	//prodPtr := flag.Bool("conf12", false, "a bool")
	//flag.Parse()
	//if *prodPtr {
	//	fmt.Println(conf.AppName)
	//	fmt.Println(conf.Production)
	//}

	//fmt.Println(conf.AppName)
	//fmt.Println(conf.Production)
}
