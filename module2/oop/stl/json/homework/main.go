package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string
	Followers int
}

func (p Profile) MarshalJSON() ([]byte, error) {
	return []byte(fmt.Sprintf(`{"f_count": "%d"}`, p.Followers)), nil
}

type Age int

type Student struct {
	Firstname, Lastname string
	Age                 int
	Profile             Profile
}

func main() {

	john := &Student{
		Firstname: "John",
		Lastname:  "Doe",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			Followers: 1975,
		},
	}

	johnJSON, _ := json.MarshalIndent(john, "", " ")

	fmt.Println(string(johnJSON))
}
