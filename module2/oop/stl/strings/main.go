package main

import (
	"fmt"
	"strconv"
	"strings"
)

type Cache struct {
	data map[string]*User
}

func NewCache() *Cache {
	return &Cache{data: make(map[string]*User, 100)}
}

func (c *Cache) Set(key string, u *User) *Cache {
	c.data[key] = u
	return c

}

func (c *Cache) Get(key string) *User {
	return c.data[key]
}

type User struct {
	ID       int
	Nickname string
	Email    string
}

func main() {
	var users []*User
	var nicknames []string
	emails := []string{"robpike@gmail.com", "davecheney@gmail.com", "bradfitzpatrick@email.ru", "eliben@gmail.com", "quasilyte@mail.ru"}
	for i, v := range emails {
		nicknames = strings.Split(v, "@")
		t := strconv.Itoa(i + 1)
		nicknames[1] = t
		nicknames1 := strings.Join(nicknames, ":")
		users = append(users, &User{
			ID:       i + 1,
			Nickname: nicknames1,
			Email:    emails[i],
		})
		fmt.Println(nicknames)
	}
	cache := NewCache()
	for i, v := range users {
		_ = i
		cache.Set(v.Nickname, v)
		// Положить пользователей в кэш с ключом Nickname:userid
		// ...
	}
	fmt.Println(*cache)
	keys := []string{"robpike:1", "davecheney:2", "bradfitzpatrick:3", "eliben:4", "quasilyte:5"}
	for i := range keys {
		fmt.Println(cache.Get(keys[i]))
	}
}
