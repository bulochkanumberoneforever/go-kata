package main

import "testing"

func TestAverage(t *testing.T) {
	values := [][]float64{{1, 2}, {3, 4}, {5, 6}, {6, 7}, {7, 8}, {8, 9}, {9, 10}, {10, 11}}
	averages := []float64{1.5, 3.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5}
	for c, i := range values {
		v := average(i[0], i[1])
		if v != averages[c] {
			t.Error(
				"For", values[c],
				"expected", averages[c],
				"got", v,
			)
		}
	}
}
func TestSum(t *testing.T) {
	values := [][]float64{{1, 2}, {3, 4}, {5, 6}, {6, 7}, {7, 8}, {8, 9}, {9, 10}, {10, 11}}
	averages := []float64{3, 7, 11, 13, 15, 17, 19, 21}
	for c, i := range values {
		v := sum(i[0], i[1])
		if v != averages[c] {
			t.Error(
				"For", values[c],
				"expected", averages[c],
				"got", v,
			)
		}
	}
}

func TestDivide(t *testing.T) {
	values := [][]float64{{2, 2}, {4, 2}, {5, 2}, {48, 8}, {12, 4}, {81, 9}, {100, 10}, {121, 11}}
	averages := []float64{1, 2, 2.5, 6, 3, 9, 10, 11}
	for c, i := range values {
		v := divide(i[0], i[1])
		if v != averages[c] {
			t.Error(
				"For", values[c],
				"expected", averages[c],
				"got", v,
			)
		}
	}
}
