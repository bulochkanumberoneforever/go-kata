package main

import "testing"

func BenchmarkNewFilterText(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText(data[i])
		}
	}
}

func BenchmarkNewFilterText1(b *testing.B) {
	ft := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			ft.SanitizeText1(data[i])
		}
	}
}
