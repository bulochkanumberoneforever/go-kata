package main

import "testing"

func BenchmarkStandartJson(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkLibraryJson(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}
