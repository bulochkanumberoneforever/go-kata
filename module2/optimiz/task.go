package main

// This file was generated from JSON Schema using quicktype, do not modify it directly.
// To parse and unparse this JSON data, add this code to your project and do:
//
//    pets, err := UnmarshalPets(bytes)
//    bytes, err = pets.Marshal()

import (
	"encoding/json"
	"fmt"
	"github.com/json-iterator/go"
	"os"
)

var jsonData, _ = os.ReadFile("jsonData.json")

type Pets []Pet

func UnmarshalPets(data []byte) (Pets, error) {
	var r Pets
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

func UnmarshalPets2(data []byte) (Pets, error) {
	var r Pets
	err := jsoniter.Unmarshal(data, &r)
	return r, err
}

func (r *Pets) Marshal2() ([]byte, error) {
	return jsoniter.Marshal(r)
}

type Pet struct {
	ID        float64    `json:"id"`
	Category  Category   `json:"category"`
	Name      *string    `json:"name,omitempty"`
	PhotoUrls []string   `json:"photoUrls,omitempty"`
	Tags      []Category `json:"tags"`
	Status    Status     `json:"status"`
}

type Category struct {
	ID   int64  `json:"id"`
	Name string `json:"name"`
}

type Status string

const (
	Sold Status = "sold"
)

func main() {
	jsonData1, _ := UnmarshalPets(jsonData)
	_, _ = jsonData1.Marshal()
	fmt.Println(jsonData1)

	jsonData2, _ := UnmarshalPets2(jsonData)
	_, _ = jsonData2.Marshal2()
	fmt.Println(jsonData2)

}
