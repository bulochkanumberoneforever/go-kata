package benching

import (
	"github.com/brianvoe/gofakeit/v6"
	"math/rand"
	"testing"
)

type User struct {
	ID       int64
	Name     string `fake:"{firstname}"`
	Products []Product
}

type Product struct {
	UserID int64
	Name   string `fake:"{sentence:3}"`
}

func MapUserProducts(users []User, products []Product) []User {
	// Проинициализируйте карту продуктов по айди пользователей
	users = genUsers() // получаем пользователей
	products = genProducts()
	for i, user := range users {
		for _, product := range products { // избавьтесь от цикла в цикле
			if product.UserID == user.ID {
				users[i].Products = append(users[i].Products, product)
			}
		}
	}

	return users
}

func MapUserProducts2(users []User, products []Product) []User {
	m := make(map[int64][]Product)
	var i int
	for i = range products { // проходимся по продуктам 1 раз
		m[products[i].UserID] = append(m[products[i].UserID], products[i]) // записываем продукты в map по UserID
	}

	for i = range users { // итерируем пользователей один раз
		if v, ok := m[users[i].ID]; ok { // проверяем есть ли продукты с таким user.ID, оптимизировали, чтобы не проходить в цикле
			users[i].Products = v // если ok, записываем продукты пользователя в поле Products
		}
	}
	return users
}

func BenchmarkInsertIntMap1(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts(genUsers(), genProducts())
	}
}

func BenchmarkInsertIntMap2(b *testing.B) {
	for i := 0; i < b.N; i++ {
		MapUserProducts2(genUsers(), genProducts())
	}
}

func genProducts() []Product {
	products := make([]Product, 1000)
	for i, product := range products {
		_ = gofakeit.Struct(&product)
		product.UserID = int64(rand.Intn(100) + 1)
		products[i] = product
	}

	return products
}

func genUsers() []User {
	users := make([]User, 100)
	for i, user := range users {
		_ = gofakeit.Struct(&user)
		user.ID = int64(i)
		users[i] = user
	}

	return users
}
