module benching

go 1.20

require github.com/brianvoe/gofakeit/v6 v6.20.1

require (
	github.com/mdigger/translit v0.0.0-20190913173519-84222363843c // indirect
	golang.org/x/exp v0.0.0-20230213192124-5e25df0256eb // indirect
)
