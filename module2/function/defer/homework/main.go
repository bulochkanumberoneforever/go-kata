package main

import (
	"errors"
	"fmt"
)

type MergeDictsJob struct {
	Dicts      []map[string]string
	Merged     map[string]string
	IsFinished bool
}

var (
	errNotEnoughDicts = errors.New("at least 2 dictionaries are required")
	errNilDict        = errors.New("nil dictionary")
)

func ExecuteMergeDictsJob(job *MergeDictsJob) (*MergeDictsJob, error) {
	defer func() {
		job.IsFinished = true
	}()
	if len(job.Dicts) < 2 {
		return job, errNotEnoughDicts
	} else {
		job.Merged = make(map[string]string)
		for _, v := range job.Dicts {
			if v == nil {
				return job, errNilDict
				break
			} else {
				for k, v1 := range v {
					job.Merged[k] = v1
				}
			}
		}
		return job, nil
	}
}

func main() {
	fmt.Print(ExecuteMergeDictsJob(&MergeDictsJob{Dicts: []map[string]string{{"a": "b"}, nil}}))
}
