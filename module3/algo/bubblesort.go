package algo

func BubbleSort(data []int) []int {
	for range data {
		for j := 0; j < len(data); j++ {
			if j == len(data)-1 {
				break
			}
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
			}
		}
	}

	return data
}

func BubbleSort2(data []int) []int {
	for i := range data {
		for j := 0; j < len(data)-i-1; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
			}
		}
	}

	return data
}

func BubbleSort3(data []int) []int {
	swapped := true
	i := 1
	for swapped {
		swapped = false
		for j := 0; j < len(data)-1; j++ {
			if data[j] > data[j+1] {
				data[j], data[j+1] = data[j+1], data[j]
				swapped = true
			}
		}
		i++
	}

	return data
}
