package algo

import "testing"

func BenchmarkBubbleSort(b *testing.B) {
	data := []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}
}

func BenchmarkBubbleSort2(b *testing.B) {
	data := []int{55, 34, 21, 13, 8, 5, 3, 2, 1, 1}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}
}

func BenchmarkBubbleSort3(b *testing.B) {
	data := []int{1, 1, 2, 3, 5, 8, 13, 21, 34, 55}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		BubbleSort(data)
	}
}
