package main

import (
	"errors"
	"fmt"
)

type Post struct {
	body          string
	publishedData int
	next          *Post
}

type Feed struct {
	length int
	start  *Post
	end    *Post
}

func (f *Feed) Append(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
		f.end = newPost
	} else {
		lastPost := f.end
		lastPost.next = newPost
		f.end = newPost
	}
	f.length++
}

func (f *Feed) Remove(publishedData int) {
	if f.length == 0 {
		panic(errors.New("Feed is empty!"))
	}

	var previousPost *Post
	currentPost := f.start
	for currentPost.publishedData != publishedData {
		if currentPost.next == nil {
			panic(errors.New("There is no such post!"))
		}
		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next
	f.length--
}

func (f *Feed) Insert(newPost *Post) {
	if f.length == 0 {
		f.start = newPost
	} else {
		var previousPost *Post
		currentPost := f.start

		for currentPost.publishedData < newPost.publishedData {
			previousPost = currentPost
			currentPost = currentPost.next
		}
		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

func (f *Feed) Inspect() {
	if f.length == 0 {
		panic(errors.New("Feed is empty"))
	}
	currentPost := f.start
	fmt.Println("_______________________")
	fmt.Println("Feed length:", f.length)
	for i := 0; i < f.length; i++ {
		fmt.Printf("item: %d - & %v\n", i, currentPost)
		currentPost = currentPost.next
	}
	fmt.Println("_________________________")
}

func main() {
	timeRightNow := 1
	f := &Feed{}
	p1 := &Post{
		body:          "First Post",
		publishedData: timeRightNow,
	}

	p2 := &Post{
		body:          "Second Post",
		publishedData: timeRightNow + 20,
	}

	p3 := &Post{
		body:          "Third Post",
		publishedData: timeRightNow + 40,
	}
	//f.Remove(1)
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)

	f.Inspect()
	f.Remove(21)
	f.Inspect()

	p4 := &Post{
		body:          "Fourth Post",
		publishedData: 2,
	}
	f.Insert(p4)

	f.Inspect()
}
